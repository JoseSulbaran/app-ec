import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";

// =======  HOME  ====== //
import Home from '../pages/Home.js';
import Pagar from '../pages/Pagar.js';
import Retorno from '../pages/Retorno.js';
import NotFound404 from '../pages/404.js';

export default function Routers(props) {
  return  (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/pagar" component={Pagar} />
        <Route exact path="/retorno" component={Retorno} />
        <Route render={ NotFound404 } />
      </Switch>
    </Router>
  )
}

