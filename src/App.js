import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import CheckIcon from '@material-ui/icons/Check';
import CustomizedSwitches from './components/switches';


const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    backgroundColor:"#000000"
  },
  toolbar: {
    flexWrap: 'wrap',
    alignText:"center"
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor:
      theme.palette.grey[400]
  },
  containerr :{
    backgroundColor:
      theme.palette.grey[200]
  }
}));

const plans = [
    'Lorem Ipsum is simply dummy text of the printing.',
    'Lorem Ipsum is simply dummy text of the printing.',
    'Lorem Ipsum is simply dummy text of the printing.',
    'Lorem Ipsum is simply dummy text of the printing.',
];


export default function Pricing(props) {
  const classes = useStyles();

  return (
    <div className={classes.containerr}>
      <CssBaseline />
      <AppBar position="static"  elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography align="center" variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
            Mag.
          </Typography>
        </Toolbar>
      </AppBar>
      {/* Hero unit */}
      <Container maxWidth="sm" component="main" className={classes.heroContent}>
        <CustomizedSwitches {...props} /> 
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={1} alignItems="center" elevation={12} >
          <Grid item  xs={12} sm={12} md={12}>
            <Card elevation={5}  > 
              <CardContent>
                <div className={classes.cardPricing}>
                  <Typography variant="h4" color="textPrimary" >
                    S/ 
                  </Typography>
                  <Typography component="h1" variant="h2" color="textPrimary" style={{margin:"0em 0.1em"}}>
                    59
                  </Typography>
                  <Typography variant="h5" color="textPrimary">
                    /AL MES
                  </Typography>
                </div>
                <Typography variant="h7" align="center" component="p" style={{ fontWeight: "bold", }}>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Typography>
                <Divider style={{marginTop:15}} />
                {plans.map((line) => (
                  <List component="nav" aria-label="main mailbox folders">
                    <ListItem>
                      <ListItemIcon>
                        <CheckIcon />
                      </ListItemIcon>
                      <ListItemText primary={line} />
                    </ListItem>
                    <ListItem>
                      <ListItemIcon>
                        <CheckIcon />
                      </ListItemIcon>
                      <ListItemText secondary={line}  />
                    </ListItem>
                  </List>
                ))}
        
              </CardContent>
              <CardActions style={{padding:0}}>
                <Button fullWidth variant='contained' color="primary" style={{backgroundColor:"#000000"}}>
                  Suscribirme
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Container>  
      {/* Footer */}
      <Container maxWidth="md" component="footer" className={classes.footer}>
        <Typography align="center" variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
          Mag.
        </Typography>
      </Container>
      {/* End footer */}
    </div>
  );
}
