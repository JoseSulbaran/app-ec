
const getDataSuperSearch = ({ dashboard }) => 
	dashboard.dataSuperSearch

const getMessageError = ({ dashboard }) =>
	dashboard.messageError

const getMessageSuccessStatic = ({ dashboard }) => 
	dashboard.messageSuccessStatic

const getMessageInfoStatic = ({ dashboard }) => 
	dashboard.messageInfoStatic

const getDataUserDelete = ({ dashboard }) => 
	dashboard.infoUserDelete

const getItemMenu = ({ dashboard }) => 
	dashboard.activeItem

const getDeleteSuperSearch = ({ dashboard }) => 
	dashboard.deleteSuperSearch

const getMessageGotBack = ({ dashboard }) => 
	dashboard.messageGotBack

// MENU //
const getMenuItem = ({ menuState }) =>
  menuState.menu

const getUserInfo = ({ menuState }) =>
  menuState.userInfo

const getUserSignInContainer= ({ menuState }) =>
  menuState.signinContainer

export {
  getDataSuperSearch,
  getMessageError,
  getMessageSuccessStatic,
  getMessageInfoStatic,
  getDataUserDelete,
  getItemMenu,
  getDeleteSuperSearch,
  getMessageGotBack,
  getMenuItem,
  getUserInfo,
  getUserSignInContainer
};