import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
//import { createLogger } from 'redux-logger';
import rootReducer from './reducers';

//const logger = createLogger();
const saga = createSagaMiddleware();

const store = createStore(
  rootReducer,
  undefined,
  applyMiddleware(saga)
);



export default store;