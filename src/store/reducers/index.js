import { combineReducers } from 'redux';
import stateReducer from './state';

const rootReducer = combineReducers({
	state: stateReducer
});

export default rootReducer;