import * as ACTIONS from '../../actions'

const initialState = {
  plan : {
    title:"Plan Estándar",
    price:"29"
  }
}

const applyAddState = (state, action) => ({
  ...state,
  ...action.data
})

function menuReducer(state=initialState, action) {
  switch (action.type) {
    case ACTIONS.SET_INFORMATION :
      return applyAddState(state, action)
    default:
      return state
  }
}
export default menuReducer
