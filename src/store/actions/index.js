export const SET_INFORMATION = 'SET_INFORMATION';

const setDataState = (text) => ({
	type:SET_INFORMATION,
	data: text
});

export { 
 setDataState
};
