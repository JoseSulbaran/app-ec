import React from "react";
import {
  AppBar,
  CssBaseline,
  Toolbar,
  Typography,
  Container,
  Avatar,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import useRetorno from "../hoock/useRetorno";
import { compose } from "redux";
import { connect } from "react-redux";
import * as setStateRedux from "../store/actions";
import DoneIcon from "@material-ui/icons/Done";

export function _Retorno(props) {
  const classes = useStyles();
  const { handleBack, plan, ...state } = useRetorno(props);

  return (
    <div className={classes.containerr}>
      <CssBaseline />
      <AppBar position="static" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography
            align="center"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            Mag.
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="sm" component="main" className={classes.heroContent}>
        <Avatar className={classes.avater}>
          <DoneIcon style={styles.icon} />
        </Avatar>
      </Container>
      <Container maxWidth="md" component="main">
        <Typography align="center" style={styles.title}>
          Bienvenido, has adquirido el
        </Typography>
        <Typography align="center" style={styles.subTitle1}>
          {plan.title}
        </Typography>
        <Typography align="center" style={styles.subTitle2}>
          S/ {plan.price} al mes
        </Typography>
      </Container>
      <Container
        maxWidth="sm"
        component="main"
        style={{ paddingTop: 15, flex: 1 }}
      >
        <Typography align="center" style={{ color: "#000", fontSize: 14 }}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum is simply dummy text of the printing and
          typesetting industry.
        </Typography>
      </Container>
      <div style={{ textAlign: "center", flex: 1 }}>
        <a style={styles.plan} onClick={() => handleBack()}>
          Ir a ver mi plan
        </a>
      </div>
      <div style={{ textAlign: "center", paddingBottom: 30 }}>
        <a style={styles.mag} onClick={() => handleBack()}>
          Ir a mag pe
        </a>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  data: state,
});

const mapDispatchToProps = (dispatch) => ({
  setDataState: (data) => dispatch(setStateRedux.setDataState(data)),
});

const Retorno = compose(connect(mapStateToProps, mapDispatchToProps))(_Retorno);

export default Retorno;

const useStyles = makeStyles((theme) => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    backgroundColor: "#000000",
  },
  toolbar: {
    flexWrap: "wrap",
    alignText: "center",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  heroContent: {
    padding: "4em 0em",
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.grey[400],
  },
  containerr: {
    backgroundColor: theme.palette.grey[200],
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
  avater: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    color: "#fff",
    backgroundColor: "#fff",
  },
}));

const styles = {
  icon: {
    fontSize: 90,
    color: "rgb(115, 102, 102",
  },
  title: {
    fontSize: 13,
    fontWeight: "bold",
  },
  subTitle1: {
    paddingTop: 10,
    color: "#000",
    fontSize: 25,
    fontWeight: "bold",
  },
  subTitle2: {
    color: "#000",
    fontSize: 18,
    fontWeight: "bold",
  },
  plan: {
    color: "#000",
    fontSize: 16,
    fontWeight: "bold",
    textDecoration: "underline",
    cursor: "pointer",
  },
  mag: {
    color: "#000",
    fontSize: 14,
    textDecoration: "underline",
    cursor: "pointer",
  },
};
