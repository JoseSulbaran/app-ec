import React from "react";
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  Container,
  TextField,
  CardContent,
  Grid,
  IconButton,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { plans } from "../constants";
import { ErrorFields } from "../components/FieldsMessage"
import { validateFieldCards } from "../constants/validator"
import usePagar from "../hoock/usePagar";
import * as setStateRedux from "../store/actions";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

export function validator(_props) {
  let { is_valid, msg } = validateFieldCards({
    numberCards: _props.numberCards,
    numberCVV: _props.numberCVV,
    nameHolder: _props.nameHolder
  })
  return { is_valid, msg } 
}

export function _Pagar(props) {
  let msgErrorField = {}
  const classes = useStyles();
  const {
    handleForm,
    handleBack,
    handleNext,
    handlePagar,
    plan,
    submit,
    ...state
  } = usePagar(props);

  if (submit) {
    msgErrorField = validator(state).msg
  }

  return (
    <div className={classes.containerr}>
      <AppBar position="static" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            aria-label="Regresar"
            size="small"
            onClick={() => handleBack()}
          >
            <ArrowBackIcon style={{ color: "#fff" }} />
          </IconButton>
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            Tus datos.
          </Typography>
        </Toolbar>
      </AppBar>
      <Container
        maxWidth="md"
        component="main"
        className={classes.heroContent}
        style={{ flex: 1 }}
      >
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <Typography
                  variant="subtitle1"
                  color="textPrimary"
                  style={styles.title}
                >
                  Nombre y aplellidos
                </Typography>
                <TextField
                  variant="outlined"
                  fullWidth={true}
                  size="small"
                  defaultValue={state.nameHolder}
                  onChange={({ target: { value } }) => handleForm({ nameHolder: value })}
                  style={styles.input}
                  error={msgErrorField?.nameHolder && true}
                  helperText={msgErrorField?.nameHolder &&  <ErrorFields msg={msgErrorField.nameHolder} />}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <Typography
                  variant="subtitle1"
                  color="textPrimary"
                  style={styles.title}
                >
                  Número de tarjeta
                </Typography>
                <TextField
                  variant="outlined"
                  fullWidth={true}
                  style={styles.input}
                  size="small"
                  placeholder="---- ---- ---- ----"
                  defaultValue={state.numberCards}
                  onChange={({ target: { value } }) => handleForm({ numberCards: value })}
                  style={styles.input}
                  error={msgErrorField?.numberCards && true}
                  helperText={msgErrorField?.numberCards &&  <ErrorFields msg={msgErrorField.numberCards} />}
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography
                  variant="subtitle1"
                  color="textPrimary"
                  style={styles.title}
                >
                  F. Expira
                </Typography>
                <TextField
                  fullWidth={true}
                  variant="outlined"
                  style={styles.input}
                  size="small"
                  placeholder="MM/AA"
                  defaultValue={state.exp}
                  onChange={({ target: { value } }) => handleForm({ exp: value })}
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography
                  variant="subtitle1"
                  color="textPrimary"
                  style={styles.title}
                >
                  CVC
                </Typography>
                <TextField
                  fullWidth={true}
                  variant="outlined"
                  style={styles.input}
                  size="small"
                  defaultValue={state.numberCVV}
                  onChange={({ target: { value } }) => handleForm({ numberCVV: value })}
                  style={styles.input}
                  error={msgErrorField?.numberCVV && true}
                  helperText={msgErrorField?.numberCVV &&  <ErrorFields msg={msgErrorField.numberCVV} />}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <Button
                  onClick={() => validator(state).is_valid ? handleNext() : !submit ? handleForm({ submit: true }) : !validator(state).is_valid  ?  null :  handleNext() }
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{ backgroundColor: "#000000", padding: "1em 0em" }}
                >
                  Pagar S/ {plan.price}
                </Button>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Container>
      <footer className={classes.footer}>
        <Container maxWidth="md">
          <div style={styles.footerContainer}>
            <Typography style={styles.plan}>{plan.title}</Typography>
            <Typography style={styles.plan}>S/ {plan.price} al mes</Typography>
          </div>
          <div style={{ textAlign: "center", padding: "1em 0em" }}>
            <a style={styles.link} onClick={() =>handlePagar({plan: plans.filter((item) => item.title !== plan.title)[0]})}>
              Cambiar a {plans.filter((item) => item.title !== plan.title)[0].title}
            </a>
          </div>
        </Container>
      </footer>
    </div>
  );
}

const mapStateToProps = (state) => ({
  data: state,
});

const mapDispatchToProps = (dispatch) => ({
  setDataState: (data) => dispatch(setStateRedux.setDataState(data)),
});

const Pagar = compose(connect(mapStateToProps, mapDispatchToProps))(_Pagar);

export default Pagar;


const useStyles = makeStyles((theme) => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    backgroundColor: "#000000",
  },
  toolbar: {
    flexWrap: "wrap",
    alignText: "center",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    paddingTop: "1.5em",
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid #ffffff`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: `#ffffff`,
  },
  containerr: {
    backgroundColor: theme.palette.grey[200],
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
}));

const styles = {
  icon: {
    fontSize: 90,
    color: "rgb(115, 102, 102",
  },
  title: {
    fontSize: "12px",
    fontWeight: "bold",
  },
  plan: {
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
  },
  footerContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  link: {
    color: "#000",
    fontSize: 14,
    textDecoration: "underline",
    cursor: "pointer",
  },
  input: {
    backgroundColor: "#fff"
  },
};
