import React from "react";
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  Container,
  Card,
  CardActions,
  CardContent,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import CheckIcon from '@material-ui/icons/Check';
import CustomizedSwitches from "../components/switches";
import {plans,  options} from "../constants";
import useHome from "../hoock/useHome";
import * as setStateRedux from "../store/actions";

export function _Home(props) {
  const classes = useStyles();
  const { handleHomeForm, handleNext, plan, ...state } = useHome(props);

  return (
    <div className={classes.containerr}>
      <AppBar position="static" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography
            align="center"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            Mag.
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="sm" component="main" className={classes.heroContent}>
        <CustomizedSwitches
          handleHomeForm={handleHomeForm}
          plan={plan}
          plans={plans}
        />
      </Container>
      <Container maxWidth="md" component="main" style={{ flex: 1 }}>
        <Grid container spacing={1} alignItems="center" elevation={12}>
          <Grid item xs={12} sm={12} md={12}>
            <Card elevation={5}>
              <CardContent>
                <div className={classes.cardPricing}>
                  <Typography variant="h4" color="textPrimary">
                    S/
                  </Typography>
                  <Typography
                    component="h1"
                    variant="h2"
                    color="textPrimary"
                    style={{ margin: "0em 0.1em" }}
                  >
                    {plan.price}
                  </Typography>
                  <Typography variant="h5" color="textPrimary">
                    /AL MES
                  </Typography>
                </div>
                <Typography
                  variant="subtitle1"
                  align="center"
                  style={{ fontSize: 13, fontWeight: "bold", lineHeight: 1.1 }}
                >
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry.
                </Typography>
                <Divider style={{ marginTop: 15 }} />
                {options.map((line, key) => (
                  <List component="nav" key={key} aria-label="main mailbox folders">
                    <ListItem style={{padding:"0em 0em"}}>
                      <ListItemIcon>
                        <CheckIcon />
                      </ListItemIcon>
                        { (plan.price === plans[0].price && key < 2 || plan.price === plans[1].price ) &&
                         <ListItemText {...{primary : line}}  />
                        }
                        { plan.price === plans[0].price  && key > 1  &&
                         <ListItemText {...{secondary : line}} />
                        }
                    </ListItem>
                  </List>
                ))}
              </CardContent>
              <CardActions style={{ padding: 0 }}>
                <Button
                  onClick={() => handleNext()}
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{
                    backgroundColor: "#000000",
                    padding: "1em 0em",
                    color: "#ffffff",
                  }}
                >
                  Suscribirme
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Container>
      <footer className={classes.footer}>
        <Container maxWidth="md" component="footer">
          <Typography
            align="center"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            Mag.
          </Typography>
        </Container>
      </footer>
    </div>
  );
}

const mapStateToProps = (state) => ({
  data: state,
});

const mapDispatchToProps = (dispatch) => ({
  setDataState: (data) => dispatch(setStateRedux.setDataState(data)),
});

const Home = compose(connect(mapStateToProps, mapDispatchToProps))(_Home);

export default Home;


const useStyles = makeStyles((theme) => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    backgroundColor: "#000000",
  },
  toolbar: {
    flexWrap: "wrap",
    alignText: "center",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  heroContent: {
    padding: "1.5em 0em",
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.grey[400],
  },
  containerr: {
    backgroundColor: theme.palette.grey[200],
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
}));