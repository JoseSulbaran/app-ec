import { re_cvv /*re_cards, re_nameHolder*/ } from "./regExp"

export function validateFieldCards(props) {
  const { numberCards, numberCVV, nameHolder, month, year } = props
  let msg = {},
    is_valid = true

  if (numberCards === "") {
    is_valid = false
    msg.numberCards = "Ingrese número de tarjeta"
  } // else if (numberCards && !re_cards.test(numberCards)) {
  //   is_valid = false
  //   msg.re_cards = "Número invalidos"
  // }

  if (numberCVV === "") {
    is_valid = false
    msg.numberCVV = "Ingrese número cvv"
  } else if (numberCVV && !re_cvv.test(numberCVV)) {
    is_valid = false
    msg.numberCVV = "Número invalidos"
  }

  if (nameHolder === "") {
    is_valid = false
    msg.nameHolder = "Ingrese nombre del titular"
  } // else if (nameHolder && !re_nameHolder.test(nameHolder)) {
  //   is_valid = false
  //   msg.nameHolder = "Nombre no valido"
  // }

  if (month === "") {
    is_valid = false
    msg.month = "Ingrese mes"
  }

  if (year === "") {
    is_valid = false
    msg.year = "Ingrese año"
  }
  return { is_valid, msg }
}

