export const re_cards = new RegExp(/^([0-9])$/)
export const re_cvv = new RegExp(/^([0-9]{3,4})$/)
export const re_nameHolder = new RegExp(/^([a-zA-ZáéíóúñüÁÉÍÓÚÑÜ])$/)
