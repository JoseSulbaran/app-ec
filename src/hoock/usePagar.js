// Dependencies
import { useState, useEffect, useCallback, useMemo } from "react";

export default function (props) {
  // States
  const { data } = props
  const [stateScreen, setStateScreen] = useState({
    numberCards : "", numberCVV:'', exp:'', nameHolder:"", ...data.state, submit: false
  });

  const handleForm = useCallback(_props => {
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);

  const handleNext = useCallback(_props => {
    const { setDataState } = props
    delete stateScreen.submit;
    setDataState({...stateScreen})
    props.history.push("/retorno")
  }, [stateScreen]);

  const handleBack = useCallback(_props => {
    props.history.replace("/")
  }, [stateScreen]);

  const handlePagar = useCallback(_props => {
    const { setDataState } = props
    setStateScreen((state) => ({ ...state, ..._props }));
    setDataState({...stateScreen, ..._props})
    console.log({..._props})
  }, [stateScreen]);


  const result = useMemo(() => {
    return {
      handleForm,
      handleNext,
      handleBack,
      handlePagar,
      ...stateScreen
    };
  }, [stateScreen]);
  // Return
    console.log({result})
  return result;
}
