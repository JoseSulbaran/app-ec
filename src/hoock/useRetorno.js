// Dependencies
import { useState, useEffect, useCallback, useMemo } from "react";

export default function (props) {
  // States
  const { data } = props
  const [stateScreen, setStateScreen] = useState({
    ...data.state
  });

  const handleBack = useCallback(_props => {
    props.history.replace("/")
  }, [stateScreen]);

  const result = useMemo(() => {
    return {
      handleBack,
      ...stateScreen
    };
  }, [stateScreen]);
  // Return

  return result;
}
