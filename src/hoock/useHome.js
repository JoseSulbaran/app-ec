// Dependencies
import { useState, useEffect, useCallback, useMemo, useRef } from "react";

export default function (props) {
  // States
  const { data } = props
  const [stateScreen, setStateScreen] = useState({
    ...data.state
  });

  const handleHomeForm = useCallback(_props => {
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);

  const handleNext = useCallback(_props => {
    const { setDataState } = props
    setDataState({...stateScreen})
    props.history.push("/pagar")
  }, [stateScreen]);

  const result = useMemo(() => {
    return {
      handleHomeForm,
      handleNext,
      ...stateScreen
    };
  }, [stateScreen]);
  console.log({stateScreen})
  // Return
  return result;
}
