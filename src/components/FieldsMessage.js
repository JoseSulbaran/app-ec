import React from "react"
import { Typography } from "@material-ui/core"
import { Error } from "@material-ui/icons"
import { makeStyles } from "@material-ui/core/styles"

export function ErrorFields(props) {
  let context = null
  const { msg } = props
  const classes = useStyles()
  if (msg) {
    context = (
      <>
        <Error className={classes.iconError} />
        <span>{msg}</span>
      </>
    )
  }
  return (
    <Typography component="span" variant="subtitle1" className={classes.error}>
      {context}
    </Typography>
  )
}

const useStyles = makeStyles(theme => ({
  error: {
    display: "flex",
    flexDirection: "row",
    fontSize: "10px",
    "& > * + *": {
      paddingLeft: "0.5em",
      fontWeight: "550",
    },
  },
  iconError: {
    paddingTop: "2px",
    fontSize: "12px",
  },
}))
