import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
     color: "#000",
    '&$checked': {
      transform: 'translateX(16px)',
      color: "#000",
      '& + $track': {
        backgroundColor: '#fff',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#fff',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});



export default function CustomizedSwitches(props) {
  const { handleHomeForm, plans, plan } = props
  return (
    <Typography component="div" >
      <Grid  
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid>
          <Typography variant="subtitle1" color="textPrimary" style={plan.price === plans[0].price ? {fontWeight:"bold"} : {color:'rgba(0, 0, 0, 0.54)'}}>
            {plans[0].title}
          </Typography>
        </Grid>
        <Grid >
          <IOSSwitch checked={plan.price === plans[1].price ? true : false } onChange={({ target: { name, checked } }) => handleHomeForm({plan: checked ? plans[1]:plans[0] })} name="checked" />
        </Grid>
        <Grid >
          <Typography variant="subtitle1"  color="textPrimary"  style={plan.price === plans[1].price ? {fontWeight:"bold"} : {color:'rgba(0, 0, 0, 0.54)'}}>
            {plans[1].title}
          </Typography>
        </Grid>
      </Grid>
    </Typography>

  );
}
